using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SendMailTests
{
    public class Tests
    {
        private IWebDriver driver;
        private readonly By _gmailButton = By.CssSelector("a.gb_d[href*='mail']");
        private readonly By _signInButton = By.CssSelector("div.header__aside__buttons a.button[href*='AccountChooser']");
        private readonly By _mailField = By.CssSelector("input[type='email']");
        private readonly By _inputEmailButton = By.CssSelector("button.qIypjc[type='button']");
        private readonly By _passField = By.CssSelector("div.SdBahf #password input[type='password']");
        private readonly By _inputPassButton = By.CssSelector("#passwordNext button.VfPpkd-LgbsSe");
        private readonly By _composeEmailButton = By.CssSelector("div.aic div[class='T-I T-I-KE L3']");
        private readonly By _receiverEmailField = By.CssSelector(".oj div textarea");
        private readonly By _subjectField = By.CssSelector("div[class='aoD az6'] input");
        private readonly By _textField = By.CssSelector("div.Ar div.editable");
        private readonly By _sendButton = By.CssSelector(".T-I.J-J5-Ji.aoO.T-I-atl");
        private readonly By _emailSuggestion = By.CssSelector("div[role='listbox'] div[data-hovercard-variant='mini']");
        private readonly By _sentEmailsMenuButton = By.CssSelector(".nU a[href*='sent']");
        private readonly By _sentEmailSub = By.CssSelector("span[id=':b4']");


        private const string userEmail = "shakhmatshahin@gmail.com";       
        private const string password = "shah121314";
        private const string receiverEmail = "saltando@ukr.net";
        private const string subject = "Hey";
        private const string text = "Hello world!!! it's me, your friend)";
        
        

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://google.com");
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void sendEmailTest()
        {
            WebDriverWait wait = new(driver, new TimeSpan(0, 0, 5));
            IWebElement gmailButton = wait.Until(e => e.FindElement(_gmailButton));
            gmailButton.Click();
            IWebElement signInButton = wait.Until(e => e.FindElement(_signInButton));
            signInButton.Click();
            IWebElement mailField = wait.Until(e => e.FindElement(_mailField));
            mailField.Click();
            mailField.SendKeys(userEmail);
            IWebElement continueButton = wait.Until(e => e.FindElement(_inputEmailButton));
            continueButton.SendKeys(Keys.Enter); 
            IWebElement passField = wait.Until(e => e.FindElement(_passField));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            passField.Click();
            passField.SendKeys(password);
            IWebElement inputPassButton = wait.Until(e => e.FindElement(_inputPassButton));
            inputPassButton.SendKeys(Keys.Enter);
            IWebElement composeEmailButton = wait.Until(e => e.FindElement(_composeEmailButton));
            composeEmailButton.Click();
            IWebElement receiverEmailField = wait.Until(e => e.FindElement(_receiverEmailField));
            receiverEmailField.Click();
            receiverEmailField.SendKeys(receiverEmail);
            IWebElement emailSuggestion = driver.FindElement(_emailSuggestion);
            if (emailSuggestion.Displayed)
            {
                emailSuggestion.Click();
            }
            IWebElement subjectField = wait.Until(e => e.FindElement(_subjectField));
            subjectField.Click();
            subjectField.SendKeys(subject);
            IWebElement textField = wait.Until(e => e.FindElement(_textField));
            textField.Click();
            textField.SendKeys(text);
            IWebElement sendButton = wait.Until(e => e.FindElement(_sendButton));
            sendButton.SendKeys(Keys.Enter);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(4);
            IWebElement sentEmailsMenuButton = driver.FindElement(_sentEmailsMenuButton);
            sentEmailsMenuButton.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(4);
            Assert.That(driver.FindElement(_sentEmailSub).Text, Is.EqualTo(subject));

        }
    }
}
